{{ 
    config(schema='s_raw_staging')
}}

with region as (
    select 
       R_REGIONKEY      as region_id,
       R_NAME           as region_name

    from dev.s_raw_data.region
)

select * from region